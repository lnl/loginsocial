<?php

namespace LocknLoad\LoginSocial;

use Illuminate\Support\ServiceProvider;

class LoginSocialProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__. '/LnlLogin.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton('LnlLogin', function () {
            return new LnlLogin(); 
        });

    }
}
