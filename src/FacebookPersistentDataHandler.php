<?php

namespace LocknLoad\LoginSocial;

use Facebook\Exceptions\FacebookSDKException;
use Session;

/**
 * Class FacebookSessionPersistentDataHandler
 *
 * @package Facebook
 */
class FacebookPersistentDataHandler implements \Facebook\PersistentData\PersistentDataInterface {
    /**
     * @var string Prefix to use for session variables.
     */
    protected $sessionPrefix = 'FBRLH_';

    /**
     * Init the session handler.
     *
     * @param boolean $enableSessionCheck
     *
     * @throws FacebookSDKException
     */
    public function __construct($enableSessionCheck = true)
    {
        if ($enableSessionCheck && session_status() !== PHP_SESSION_ACTIVE) {
            throw new FacebookSDKException(
                'Sessions are not active. Please make sure session_start() is at the top of your script.',
                720
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function get($key)
    {
        return Session::get($this->sessionPrefix . $key, null);
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value)
    {
        Session::put($this->sessionPrefix . $key, $value);
    }
}
