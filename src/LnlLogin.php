<?php
namespace LocknLoad\LoginSocial;

Use Facebook\Facebook;

class LnlLogin {

    private static $fb = null;
    private static $appId = null; //Facebook App ID
    private static $appSecret = null; // Facebook App Secre
    private static $return_url = null;  //return url (url to script)
    private static $fbPermissions = ['public_profile','email','user_friends'];  //Required facebook permissions

    public function __construct() {
        self::startFb();
    }
    
    static function startFb() {

        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start(); 
        }

        self::$return_url = env('FACEBOOK_RETURN_URL');  //return url (url to script)
        self::$appId = env('FACEBOOK_APP_ID');
        self::$appSecret = env('FACEBOOK_APP_SECRET');
       
        self::$fb = new Facebook([
          'app_id' => self::$appId,
          'app_secret' => self::$appSecret,
          'persistent_data_handler' => new FacebookPersistentDataHandler()
        ]);

    }

    static function fbLoginUrl(){
        self::startFb();

        $helper = self::$fb->getRedirectLoginHelper();

        return  $helper->getLoginUrl(self::$return_url, self::$fbPermissions);
    }

    static function getFb(){
        return self::$fb;
    }
   
    static function fbValidaLogin($code){
        self::startFb();

        $helper = self::$fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        
        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        
        $response = self::$fb->get('/me?fields=email,first_name,last_name,picture', $accessToken->getValue());
    
        return array('token' => $accessToken->getValue(), 'user' => $response->getGraphUser());
    }

}
